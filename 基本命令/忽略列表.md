## 忽略的意思
有时候我们想把文件放在Git目录下，但是又不想要提交到版本库里，比如`node_modules`目录。

## .gitignore文件
在线浏览`gitignore`文件，https://github.com/github/gitignore

### 忽略文件的原则
- 忽略操作系统自动生成的文件，比如缩略图等；
- 忽略编译生成的中间文件、可执行文件等，也就是如果一个文件是通过另一个文件自动生成的，那自动 生成的文件就没必要放进版本库，比如Java编译产生的.class文件；
- 忽略你自己的带有敏感信息的配置文件，比如存放口令的配置文件。

## gitignore文件示范
```
# Windows:
Thumbs.db
ehthumbs.db
Desktop.ini

# Python:
*.py[cod]
*.so
*.egg
*.egg-info
dist
build

# My configurations:
db.ini
deploy_key_rsa
```