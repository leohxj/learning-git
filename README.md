#learning-git

test目录下是用来测试命名的文件。

## 初级掌握的内容
- 学会安装，配置git
- 学会git init本地创建目录
- 学会本地仓库链接远程仓库, git remote命令
- 学会git clone远程仓库
- 理解working dir, index, head三个区域
- 学会git status查看当前状态
- 学会git add, git commit 提交到本地head区域
- 学会git push推送到远程仓库