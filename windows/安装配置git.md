git是一种版本控制工具，github, oschina等提供的是基于git的管理服务。

# 安装
下载地址: [git官网](http://git-scm.com/downloads)。

安装过程中安装默认选项选择即可，成功安装后，在右键菜单中应该多出了一些关于git的选项，比如`git GUI`和`git bash`两个选项。通过`git bash`，我们就可以使用git了。

此时可以使用的是https协议，非ssh协议。