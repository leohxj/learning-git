ssh是一种专门为远程登录和网络服务的一种安装协议，我们在使用github, oschina等使用git的代码管理服务时，经常会使用到ssh。

使用ssh的好处是具有安全的身份认证。这样以后每次pull, push的时候就不需要再次输入密码了。

# 如何生成
安装完git之后，我们可以使用`git bash`来创建ssh。在`User/.ssh/`下使用命名`ssh-keygen -t rsa -C "your_email@example.com"`，会创建出`id_rsa`和`id_rsa.pub`两个文件。

将`id_rsa.pub`添加到服务器端的相应配置位置即可，客户端使用`id_rsa`即可。

# 测试连接
`比如github的话，使用`ssh -T git@github.com`。

# clone项目
使用命名: `git clone git@git.oschina.net:leohxj/learning-git.git`这样。

## 参考资料
- [github ssh guide](https://help.github.com/articles/generating-ssh-keys)