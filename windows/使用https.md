https方式每次都要输入密码，按照如下设置即可输入一次就不用再手输入密码的困扰而且又享受https带来的极速。

设置记住密码：
```
git config --global credential.helper cache
```

如果想加上失效时间，可以这样做：
```
git config credential.helper 'cache --timeout=3600'
```

这样就设置一个小时之后失效。

上面的方法，我在git 1.8版本上没成功，SOF上查到一下描述：
>Since git 1.7.9 (released in late January 2012), there is a neat mechanism in git to avoid having to type your password all the time for HTTP / HTTPS, called credential helpers. (Thanks to dazonic for pointing out this new feature in the comments below.)

> With git 1.7.9 or later, you can just do:
git config --global credential.helper cache
... which tells git to keep your password cached in memory for (by default) 15 minutes. You can set a longer timeout with:

>git config --global credential.helper "cache --timeout=3600"
(That example was suggested in the GitHub help page for Linux.) You can also store your credentials permanently if so desired, see the other answers below.

>GitHub's help also suggests that if you're on Mac OS and used homebrew to install git, you can use the native Mac OS keystore with:

>git config --global credential.helper osxkeychain
... and there is a similar helper for Windows, called git-credential-winstore.exe. On Linux, you can use gnome-keyring.


所以在Windows下，方案是，下载[git-credential-winstore][http://gitcredentialstore.codeplex.com/]。下载之后直接运行，保证`.gitconfig`中是这样的形式:
```
[credential]
    helper = !'C:\\Users\\Hui\\AppData\\Roaming\\GitCredStore\\git-credential-winstore.exe'
```

另外使用`git config --global credential.helper store`, 经过测试也是可行的，这个等于是命令行的形式。上面的属于GUI形式。这种方式会在用户主目录下创建一个`.git-credentials`文件。（`~/.git-credentials`）。是明文保存用户名和密码。。。

# ssh切换到https
查看当前仓库使用的是什么方式，打开`git remote -v`查看。

如果你正在使用ssh而且想体验https带来的高速，那么你可以这样做： 切换到项目目录下 ：
```
cd projectfile/
# 移除远程ssh方式的仓库地址
git remote rm origin
# 增加https方式的仓库地址
git remote add origin http://git.oschina.net/username/project.git
# 修改本地连接远程的origin
git push --set-upstream origin master
```

或者简单的:
```
git remote set-url origin git@github.com:yourname/yourrepo.git
```

OK, 再次提交的时候，输入一次用户名和密码即可enjoy the speed~!


