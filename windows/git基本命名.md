配置好了git和ssh之后，我们来使用下git。

# 创建git仓库
在一个目录下使用`git init`命名，就会在此创建一个git仓库。

# 查看仓库状态
`git status`，查看当前仓库的信息。

# clone仓库
如果你需要拷贝一个仓库，使用`git clone 地址`，这个地址可以是`https`或`ssh`开头的。

# 添加文件到仓库
这个仓库是本地的，使用命名`git add filename`或者`git add .`添加所有文件。

# 提交文件到仓库
`git commit -m "提交信息"`，这个命名会把文件提交到本地的仓库中，产生一条log。

# push到远程
`git push origin local_branch:remote_branch